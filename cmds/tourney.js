/* ----------------------------------------
	This file controls all the automated
	tournament related commands
 ---------------------------------------- */

const logger  = require("../core/logger"),
	command = require("../core/command").Command,
	x = require("../core/vars"),
	dio = require("../core/dio"),
	helpers	= require("../core/helpers"),
	TOKEN 	= require("../core/tokens"),
	stripIndents = require("common-tags").stripIndents,
	zip = require("jszip"),
	http = require("https"),
	Convert = require("xml-js"),
	challonge = require("challonge-node-ng");

const client = challonge.withAPIKey(TOKEN.CHALLONGE);

let currentTourney = null,
	tNum = null,
	tPlayers = {},
	tRound = null,
	tCount = 0,
	tourneyChan = ( helpers.isDebug() ) ? x.testing : x.tourney;

// In case of reset/crash mid tourney, look for any open PB tournaments and set all the data from there
async function resumeTourney() {
	let tournies = await client.tournaments.index({ "subdomain": "pocketbotcup" }),
		openT = tournies.filter( t => t.state !== "complete");

	if (openT[0]) {
		// Set ID and count
		currentTourney = openT[0].id;

		// Let's get back the participant/round data
		let tData = await getTourneyData();

		tData.participants.forEach( obj => tPlayers[obj.participant.misc] = obj.participant.id);
		tCount = Object.keys(tPlayers).length;

		let earlyMatch = tData.matches.map( obj => obj.match )
				.filter( match => match.state === "open"),
			completeMatches = tData.matches.map( obj => obj.match )
				.filter( match => match.state === "complete");

		tRound = (earlyMatch[0]) ? earlyMatch[0].round : 1; // If we got back an open match, first should contain earliest round

		if (tRound === 3) {
			tRound = (earlyMatch[earlyMatch.length-1].round === 0) ? tRound : 4; // Unless of course Round 0 (which is the bronze match), is hiding at the end
		}

		if (tData.matches.length === completeMatches.length) tRound = 4; // If ALL matches are done, set to final round

		logger.log(`Found existing tournament: ${currentTourney} | Now resuming from Round ${tRound} with ${tCount} players.`, "OK");
	}
}

resumeTourney();

// This will create the tournament in Challonge
async function makeTourney(data) {
	let cups = await client.tournaments.index({ "subdomain": "pocketbotcup" }),
		n = cups.length+1;

	tNum = n;

	const tournament = await client.tournaments.create({
		"tournament": {
			"name": `Pocketbot Cup #${n}`,
			"url": `pocketbotcup_${n}`,
			"subdomain": "pocketbotcup",
			"description": "Welcome to the new and fully automated <strong>Pocketbot Cup</strong>! This is a weekly cup run by Pocketbot every Monday to let players enjoy a small dose of competition, while helping analyze replay data with the latest patch. If you have any questions or suggestions, talk to Mastastealth on the <a href='http://discord.gg/pockwatch'>PWG Discord</a>.",
			"hold_third_place_match": true,
			"accept_attachments": true,
			"signup_cap": 8,
			"start_at": new Date(Date.now() + (60 * 60 * 1000)), // Start in 1h
			"show_rounds": true,
			"game_id" : 54849,
			"game_name": "Tooth and Tail",
			"check_in_duration": 20 // minutes
		}
	});

	logger.log(`Created tournament: ${tournament.id}`,"OK");
	currentTourney = tournament.id;

	// PB announces it
	dio.say(`:trophy: A new Pocketbot Cup has begun! Follow it on Challonge here: http://pocketbotcup.challonge.com/pocketbotcup_${n} \n\n Sign up using \`!signup\` or if you already have a challonge account, use \`!signup challonge_username\`. \n\n There are 8 slots available (and 2 backup slots). Tournament starts in 1 hour, check-ins open 15 minutes prior to start.`, data, tourneyChan);
}

function addPlayer(data, cid) {
	const stamp = ( helpers.isDebug() ) ? `__${Date.now()}` : false,
		u = { name: `${data.user}${(stamp) ? stamp : ""}`, id: data.userID };

	client.participants.create(currentTourney, u.name).then( player => {
		// Update user with Discord ID
		let udata = {
			participant: {
				name: u.name,
				misc: `${u.id}`
			}
		};

		// Set the challonge ID if they pass in an account
		if (cid) udata.participant["challonge_username"] = cid;

		client.participants.update(currentTourney, player.id, udata).then( () => {
			logger.log(`Player ${player.id} (${u.id}) entered tournament.`, "Info");
			tCount++;
			tPlayers[`${u.id}`] = player.id; // Store a local list of players

			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.competitor
			}, function(err,resp) {
				if (err) logger.log(`${err} | ${resp}`, "Error");
			});

			if (tCount >= 9) {
				dio.say(`<@${u.id}> is on standby!`, data, tourneyChan);
			} else {
				dio.say(`<@${u.id}> has entered the Cup!`, data, tourneyChan);
			}

			// Check if we've hit our player limit
			if (tCount === 10) {
				setTimeout( ()=> {
					dio.say(":trophy: We have now reached the maximum amount of participants! I will announce check-ins later on.", data, tourneyChan);
				}, 1000);
			}
		}).catch(e => {
			logger.log(e, "Error");

			dio.say("🕑 Hmm, that doesn't seem to be a valid username. Double check you spelled it correctly or just use the standard `!signup` command.", data, tourneyChan);

			// Remove the player that was just added since something went wrong
			client.participants.delete(currentTourney, player.id);
		});
	});
}

async function deletePlayer(data, cid, did) {
	try {
		await client.participants.delete(currentTourney, cid);
		dio.say(`🕑 <@${did}> has been removed from the tournament. Hope you can play next time!`, data, tourneyChan);
		delete tPlayers[cid];
	} catch(e) {
		dio.say("🕑 Removal unsuccessful, guess you're stuck playing. Forever.", data, tourneyChan);
		logger.log(`Failed for user: ${cid}`, "Error");
		logger.log(e,"Error");
	}
}

// Process check-ins and commence tournament
async function startTourney(data) {
	// Needs to be 6 or more players
	if (tCount >= 6) {
		try {
			await client.tournaments.proc_checkin(currentTourney);
			await client.participants.randomize(currentTourney);
			await client.tournaments.start(currentTourney);
			let cTourney = await getTourneyData();

			// Create dictionary with "Challonge User ID : Discord User ID" pairs
			let cPlayers = {};
			cTourney.participants.forEach( (obj) => {
				cPlayers[obj.participant.id] = obj.participant.misc;
			});

			// Find all round 1 matches and return vs. strings with <@user> pings
			let cMatches = cTourney.matches.map( obj => obj.match )
				.filter( match => match.state === "open" && match.round === 1)
				.map( match => {
					return `:regional_indicator_${match.identifier.toLowerCase()}: <@${cPlayers[match.player1_id]}> vs. <@${cPlayers[match.player2_id]}>`;
				});

			dio.say(stripIndents`Processed all players who checked in. **Let the games begin! :mega:**

			Round 1 Matches :crossed_swords::
			${cMatches.join("\n")}

			**Reminder**: You **will** need to submit replays to log your scores, so make sure to save 'em! :floppy_disk: Matches are **best of 3**.`, data, tourneyChan);

			tRound = 1; // Start round 1

			// Remove all the people with Competitor who AREN'T playing
			let cPlaying = cTourney.participants.map( obj => obj.match )
				.filter( player => player && player.seed > 8);

			Object.values(cPlaying).forEach( (p, i) => {
				setTimeout( () => {
					data.bot.removeFromRole({
						serverID: x.chan,
						userID: data.userID,
						roleID: x.competitor
					}, (err,resp) => {
						if (err) logger.log(`${err} | ${resp}`, "Error");
					});
				}, (i+1)*600);
			});

		} catch (err) {
			logger.log(err, "Error");
			dio.say("Couldn't start tournament, check console for errors.", data, tourneyChan);
		}
	} else {
		try {
			await client.tournaments.destroy(currentTourney);
			dio.say("Not enough participants entered the tournament this week, so it will be cancelled. :frowning: Gather some more folks for next week!", data, tourneyChan);
			resetTourneyVars(data);
		} catch (err) {
			logger.log(err, "Error");
			dio.say("Couldn't destroy tournament, check console for errors.", data, tourneyChan);
		}
	}
}

async function finishTourney(data, tid) {
	try {
		let tourney = (tid) ? tid : currentTourney;
		await client.tournaments.finalize(tourney);
		let cTourney = await getTourneyData();

		// Announce winners
		let winners = {};
		cTourney.participants
			.filter(p => p.participant.checked_in && !p.participant["has_irrelevant_seed"])
			.map( (obj) => {
				logger.log(`${obj.participant}`, "Info");

				// Earn WIP
				let wip = 5;

				if (obj.participant.final_rank === 1 ) { wip = 50; }
				else if (obj.participant.final_rank === 2) { wip = 30; }
				else if (obj.participant.final_rank === 3) { wip = 15; }
					
				data.userdata.transferCurrency(null, obj.participant.misc, wip, true).then( (res) => {
					logger.log(`Giving ${obj.participant.misc} ${wip} WIP`, "OK");
					if ( res.hasOwnProperty("err") ) logger.log(res.err, "Error");
				});

				let notTop3 = (obj.participant.final_rank > 3) ? `-${obj.participant.seed}` : false;
				winners[`${obj.participant.final_rank}${(notTop3) ? notTop3 : ""}`] = (obj.participant.misc) ? obj.participant.misc : obj.participant.name;
			});

		logger.log(`Winners: ${winners["1"]}, ${winners["2"]}, ${winners["3"]}`, "Info");

		dio.say(stripIndents`The tournament has come to a close! :tada: Our winners are:
			:first_place: <@${winners["1"]}> +50 ${x.emojis.wip}
			:second_place: <@${winners["2"]}> +30 ${x.emojis.wip}
			:third_place: <@${winners["3"]}> +15 ${x.emojis.wip}
			**All** other participants will receive 5 ${x.emojis.wip} as well. Thanks for playing, see you next week!`, data, tourneyChan);

		logger.log("Finished tournament.", "OK");
		resetTourneyVars(data);
	} catch (err) {
		logger.log(err, "Error");
		dio.say("Couldn't end tournament, check console for errors.", data, tourneyChan);
	}
}

// For check in OR out
function tourneyCheckIn(data, checkin=true) {
	// Get the player's challonge player ID
	const pid = tPlayers[`${data.userID}`];

	// Check for tourney
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	if (checkin) {
		client.participants.checkin(currentTourney, pid).then( () => {
			dio.say(`🕑 <@${data.userID}>, you're checked in! Good luck in your matches.`, data, tourneyChan);
		}).catch( e => {
			logger.log(e, "Error");
			dio.say("🕑 There was an error checking in. Check-in opens 15 minutes before the tournament starts.", data, tourneyChan);
		});
	} else {
		client.participants.checkout(currentTourney, pid).then( () => {
			data.bot.removeFromRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.competitor
			}, function(err,resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					return false;
				}

				dio.say(`🕑 <@${data.userID}>, you're checked out! Hopefully one of the backups will take your place or you'll end up ruining everything for everyone. :grin:`, data, tourneyChan);
			});
		}).catch( e => {
			logger.log(e, "Error");
			dio.say("🕑 There was an error checking in. Check-in opens 10 minutes before the tournament starts.", data, tourneyChan);
		});
	}
}

// Retrieves tourney data with participants and matches as well
function getTourneyData(id=currentTourney) {
	return client.tournaments.show(id, { "include_participants" : 1, "include_matches" : 1 });
}

async function checkRound(data) {
	// Check to see if previous round is over yet

	const t = await getTourneyData(),
		roundMap = [null,1,2,0,3],
		rounds = t.matches.map( obj => obj.match )
			.filter( match => match.state === "open" && match.round == roundMap[tRound]);

	logger.log(`Open rounds left: ${rounds.length}`,"Info");

	if (rounds.length === 0 && tRound < 4) {
		// Start next round
		tRound++;

		// Make arrays of players and matches
		let cPlayers = {},
			tRoundAdj = tRound;

		t.participants.forEach( (obj) => {
			cPlayers[obj.participant.id] = obj.participant.misc;
		});

		// Shift tRound so that bronze match plays before finals
		if (tRound === 3) tRoundAdj = 0; // "Round 3" becomes Bronze (0)
		if (tRound === 4) tRoundAdj = 3; // "Round 4" becomes Final (3)

		logger.log(`Starting Round ${tRoundAdj}`,"OK");

		let cMatches = t.matches.map( obj => obj.match )
			.filter( match => match.round === tRoundAdj) // Get the ones for current round
			.map( match => {
				if (match.identifier !== "3P") { // For normal matches
					return `:regional_indicator_${match.identifier.toLowerCase()}: <@${cPlayers[match.player1_id]}> vs. <@${cPlayers[match.player2_id]}>`;
				} else { // For the bronze match
					return `:third_place: <@${cPlayers[match.player1_id]}> vs. <@${cPlayers[match.player2_id]}>`;
				}
			});

		if (tRound === 2) {
			dio.say(stripIndents`**Semi-finals have begun! :mega:**

			Semi-final Matches :crossed_swords::
			${cMatches.join("\n")}

			http://pocketbotcup.challonge.com/pocketbotcup_${tNum}`, data, tourneyChan);
		} else if (tRound === 3) {
			// Finished Finals, time for bronze match
			dio.say(stripIndents`**Our finalists are ready, but will now rest as we see who takes the bronze! :mega:**
			${cMatches.join("\n")}

			http://pocketbotcup.challonge.com/pocketbotcup_${tNum}`, data, tourneyChan);
		} else if (tRound === 4) {
			// Finished Finals, time for bronze match
			dio.say(stripIndents`**:trophy: :mega: And now for the grand finals! :mega: :trophy:**
			${cMatches.join("\n")}

			Reminder: Finals are **best of 5**!`, data, tourneyChan);
		}
	} else if (tRound === 4) {
		// Finished
		finishTourney(data);
	}
}

async function updateScore(data, file) {
	// All right, everything looks legit enough. Let's assume we have a zip with
	// 3 replay files in it, did we have a winner?
	let score = (data.args[1] && data.args[1].length === 3) ? data.args[1].split("-") : false,
		replayCount = checkZip(data);

	if (replayCount < 3) {
		dio.say("This archive doesn't have the correct amount of replays.", data);
		return false;
	}

	if (!score) {
		dio.say("What was the score? Use the syntax `!score x-y`, no spaces between the number and dash.", data);
		return false;
	}

	// Check scores to be realistic
	if (parseInt(score[0]) > 3 || parseInt(score[1]) > 3) {
		dio.say("Those are some fishy scores. Double check they are correct!", data);
		return false;
	}

	// Ok, we do have a winner, commence score upload stuff!
	let winnerID,
		winnerCID,
		cTourney = await getTourneyData();

	// Set the winner
	if (score[0] > score[1]) {
		winnerID = data.userID;

		// Get the winner's challonge ID
		for (let p in tPlayers) {
			if (p === winnerID) winnerCID = tPlayers[p];
		}
	} else {
		// Welp, now I gotta find the opponent automatically.
		cTourney.matches.forEach( obj => {
			// Only check open matches
			if (obj.match.state === "open") {
				// See if person uploading is player1 or 2 of the match
				let players = [obj.match.player1_id, obj.match.player2_id];

				if (tPlayers[data.userID] == players[0]) { // Uploader is player 1, so winner is player2
					for (let w in tPlayers) { if (tPlayers[w] === players[1]) winnerID = tPlayers[w]; }
					winnerCID = players[1];
				} else if (tPlayers[data.userID] == players[1]) { // Uploader is player 2, winner is player 1
					for (let w in tPlayers) { if (tPlayers[w] === players[0]) winnerID = tPlayers[w]; }
					winnerCID = players[0];
				}
			}
		});
	}


	// Now let's look at the correct match and update it accordingly
	let match;

	cTourney.matches.forEach( obj => {
		if (obj.match.state === "open") {
			if (obj.match.player1_id == winnerCID || obj.match.player2_id == winnerCID) match = obj.match;
		}
	});

	logger.log(match.id, "Info");

	// Attach link to match
	try {
		logger.log("Attaching match replays...", "Info");
		await client.matches.add_attach(currentTourney, match.id, { match_attachment: { url: file.url, description: file.filename } });

		// Format things correctly and update score
		logger.log("Formatting score for update", "Info");
		if (score[0] > score[1] && match.player1_id == winnerCID) {
			await client.matches.update(currentTourney, match.id, { "match" : { "scores_csv":`${score[0]}-${score[1]}`, "winner_id":winnerCID } });
		} else if (score[1] > score[0] && match.player1_id == winnerCID) {
			await client.matches.update(currentTourney, match.id, { "match" : { "scores_csv":`${score[1]}-${score[0]}`, "winner_id":winnerCID } });
		} else if (score[0] > score[1] && match.player2_id == winnerCID) {
			await client.matches.update(currentTourney, match.id, { "match" : { "scores_csv":`${score[1]}-${score[0]}`, "winner_id":winnerCID } });
		} else if (score[1] > score[0] && match.player2_id == winnerCID) {
			await client.matches.update(currentTourney, match.id, { "match" : { "scores_csv":`${score[0]}-${score[1]}`, "winner_id":winnerCID } });
		}

		// Confirm score submitted
		dio.say("Score updated. :tada: Please wait for round to finish before starting your next match. You will be notified when this happens.", data);

		// TODO - v2 would be amazing to download zip, analyze replays, and add data straight into Firebase for Zenoboard to consume
		// getReplayData();

		checkRound(data);
	} catch(err) {
		logger.log(err, "Error");
		dio.say(`Dang it, something went wrong with the score submission. Try again? If you think you did everything correctly, talk to <@${x.stealth}>! :scream: \n \`\`\`Error: ${err}\`\`\``, data);
	}
}

function resetTourneyVars(data) {
	// Remove ALL competitors
	Object.keys(tPlayers).forEach( (p, i) => {
		logger.log(`Removing Competitor role from ${p}`, "OK");
		setTimeout( () => {
			data.bot.removeFromRole({
				serverID: x.chan,
				userID: p,
				roleID: x.competitor
			}, (err,resp) => {
				if (err) logger.log(`${err} | ${resp}`, "Error");
			});
		}, (i+1)*600);
	});

	// Now reset the vars
	tPlayers = {},
	tCount = 0,
	tRound = 1,
	currentTourney = null;
}

function checkZip(data, analyze=true, sendEmbed=false) {
	let dfile = data.attachments[0];

	let request = http.get(dfile.url, (response)=> {
		if (response.statusCode !== 200) {
			logger.log(`Response status was ${response.statusCode}`,"Error");
			dio.say("🕑 Error retrieving the file. :frowning:", data);
			return false;
		}

		let zdata = [];

		response.on("data", (chunk)=> {
			zdata.push(chunk);
		});

		response.on("end", function() {
			let buf = Buffer.concat(zdata);

			if (dfile.filename.endsWith(".xml") && sendEmbed) {
				analyzeReplays(data, buf, sendEmbed);
				return 0;
			}

			zip.loadAsync(buf).then( (z) => {
				let zLen = 0;

				z.forEach( async (replay) => {
					zLen++;
					const xml = await z.file(replay).async("string");
					if (analyze) analyzeReplays(data, xml, sendEmbed);
				});

				const replayCount = zLen;
				dio.del(data.messageID, data);
				return replayCount;
			}).catch( (e) => {
				logger.log(e,"Error");
			});
		});
	});

	// check for request error too
	request.on("error", function (err) {
		dio.say("🕑 Oops, I couldn't upload the replay.", data);
		logger.log(err.message, "Error");
		return false;
	});
}

async function analyzeReplays(d, xml, sendEmbed) {
	let json = await convertReplay(xml);

	try {
		const fid = d.db.balance.child("replays_v2").push(json);
		const players = json.Players.Player;

		logger.log(`Saved replay data from ${players[0].Identity.attr.Name} vs ${players[1].Identity.attr.Name} to Firebase.`, "OK");

		if (sendEmbed) replayEmbed(d,json,fid);
	} catch(e) {
		dio.say("🕑 Error analyzing the replays.", d);
		logger.log(e, "Error");
	}
}

function replayEmbed(d,json,fid) {
	const players = json.Players.Player,
		p1 = players[0],
		p2 = players[1], 
		p1Win = (p1.ArmyIndex.txt === json.Results.WinningTeam.txt) ? true : false,
		time = parseInt(json.MatchTime.txt);

	let embed = new helpers.Embed({
		title: `:crossed_swords: ${p1.Identity.attr.Name} vs ${p2.Identity.attr.Name}`,
		color: (p1Win) ? getFaction(p1.FactionId.txt).color : getFaction(p2.FactionId.txt).color,
		description: `The battle lasted ${Math.round((time/60) * 10) / 10}m, with a victory for **${(p1Win) ? p1.Identity.attr.Name : p2.Identity.attr.Name}**`,
	});

	embed.fields = [
		{
			name: `${getFaction(p1.FactionId.txt).emoji} ${p1.Identity.attr.Name}'s Deck`,
			value: `${getDeck(p1.Deck.Cards.Card)}`
		},
		{
			name: `${getFaction(p2.FactionId.txt).emoji} ${p2.Identity.attr.Name}'s Deck`,
			value: `${getDeck(p2.Deck.Cards.Card)}`
		},
		{
			name: "---",
			value: `[View on ReplayUltima](https://mastastealth.github.io/replay-ultima/#/${fid.key})`
		}
	];

	dio.sendEmbed(embed, d);
}

function getFaction(n) {
	let e, c;
	switch(n) {
	case "0":
		e = x.emojis.hopper;
		c = 0xD21E1E;
		break;
	case "1":
		e = x.emojis.bellafide;
		c = 0x148CFA;
		break;
	case "2":
		e = x.emojis.archi;
		c = 0xFFDC00;
		break;
	case "3":
		e = x.emojis.qm;
		c = 0x2ECC40;
		break;
	}
	return { emoji: e, color: c };
}

function getDeck(arr) {
	let deck = arr.map( (unit) => {
		let u = unit.attr.Data.replace("warren_","").replace("structure_","");
		return x.emojis[u];
	});

	return deck.join(" ");
}

function convertReplay(file) {
	return new Promise(function(resolve, reject) {
		try {
			const raw = JSON.parse(Convert.xml2json(file, {
				compact: true,
				spaces: 2,
				ignoreDeclaration: true,
				attributesKey: "attr",
				textKey: "txt",
			})).EventHistory;

			// Cleanup Large Input Junk we can't understand anyway
			delete raw.History;
			delete raw.attr;

			resolve(raw);
		} catch (e) {
			reject(e);
		}
	});
}

// =================== END OF FUNCTIONS =======================

// Admin/Bot Only Commands

let cmdCreate = new command("tourney", "!makecup", "Create a new tournament", (data) => {
	dio.del(data.messageID, data);
	makeTourney(data);
});

cmdCreate.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

let cmdStart = new command("tourney", "!startcup", "Starts tournament", (data)=> {
	dio.del(data.messageID, data);
	startTourney(data);
});

cmdStart.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

let cmdEnd = new command("tourney", "!endcup", "Ends tournament", (data)=> {
	dio.del(data.messageID, data);
	let tid = (data.args[1]) ? data.args[1] : false;
	finishTourney(data, tid);
});

cmdEnd.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

let cmdCheckInRemind  = new command("tourney", "!checkinremind", "Lists participants/matches in tournament", async (data) => {
	dio.del(data.messageID, data);

	dio.say(`Will all <@&${x.competitor}>s please \`!checkin\` if you can play in today's tournament? If you cannot, go ahead and \`!checkout\` instead. Also note, anyone who \`!signup\`s from this point on will be automatically checked in.`, data, tourneyChan);
});

cmdCheckInRemind.permissions = (helpers.isHeroku()) ? [x.admin, x.adminbot] : [x.admin, x.adminbot, x.combot];

// Admin/Mod Helper commands

let cmdList = new command("tourney", "!lscup", "Lists participants/matches in tournament", async (data) => {
	dio.del(data.messageID, data);

	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Attempts to get a list of all players and matches with relevant properties
	try {
		let t = await getTourneyData(),
			players = t.participants.map( (p)=> {
				return `${p.participant.name} | ${p.participant.id} - ${(p.participant.checked_in) ? "✓" : "⛔"}`;
			}),
			matches = t.matches.map( (m)=> {
				return `${m.match.identifier} - ${m.match.id} - R${m.match.round} | ${(!m.match.state == "open") ? "☑" : "☐"}`;
			});

		dio.say(`Tournament ID: ${currentTourney} \n The current competitors are: \`\`\`${players.join("\n")}\n---\n${matches.join("\n")}\`\`\``, data);
	} catch(e) {
		logger.log(e, "Error");
	}
});

cmdList.permissions = [x.admin, x.mod];

let cmdListReplays = new command("tourney", "!lsreplays", "Lists all the replay attachments from last tournament", async (data) => {
	dio.del(data.messageID, data);

	// Attempts to get a list of all players and matches with relevant properties
	try {
		let tournies = await client.tournaments.index({ "subdomain": "pocketbotcup" }),
			pocketTournies = tournies.filter( t => t.state == "complete"),
			lastTID = pocketTournies[0].id, // Is sorted latest to oldest?
			lastT = await getTourneyData(lastTID),
			matches = lastT.matches.map( (m)=> {
				return m.match.id;
			}),
			replays = matches.map( async m => {
				let a = await client.matches.index_attach(lastTID, m);
				return (a[0] && a[0].hasOwnProperty("url")) ? `<${a[0].url}>` : "";
			});

		dio.say("🕑 I'll PM you the list of all the replays once as soon as I gather them.", data);

		let replaysResolved = await Promise.all(replays);

		dio.say(`Replays for the last tournament: \n${replaysResolved.join("\n")}`, data, data.userID);
	} catch(e) {
		dio.say("🕑 Couldn't get the list of replays. :thinking:", data);
		logger.log(e, "Error");
	}
});

cmdListReplays.permissions = [x.admin, x.mod];

let cmdAddMatch = new command("tourney", "!addmatch", "Adds match to tournament", async (data) => {
	dio.del(data.messageID, data);

	try {
		let mid = data.args[1], // The match ID from challonge
			score = data.args[2], // Should be in X-Y format
			winner = (data.args[3]) ? data.args[3] : null; // Challonge user ID

		let match = await client.matches.update(currentTourney, mid, {
			match: {
				"scores_csv" : score,
				"winner_id" : winner
			}
		});

		dio.say(`Match ${match.id} updated.`, data);
		checkRound(data);
	} catch (err) {
		logger.log(err, "Error");
	}
});

cmdAddMatch.permissions = [x.admin, x.mod];

let cmdCheckRound = new command("tourney", "!round", "Force checks the current round", (data) => {
	dio.del(data.messageID, data);

	try {
		checkRound(data);
	} catch(e) {
		logger.log(e, "Error");
	}
});

cmdCheckRound.permissions = [x.admin, x.mod];

let cmdDQ = new command("tourney", "!dq", "Forcefully removes player from tournament", (data) => {
	dio.del(data.messageID, data);

	let player = helpers.getUser(data.args[1]);

	// Check for an actual tournament
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Check if you already signed up
	if (!tPlayers.hasOwnProperty(player)) {
		dio.say("🕑 This user isn't even part of the tournament.", data, tourneyChan);
		return false;
	}

	deletePlayer(data, tPlayers[player], player);
});

cmdDQ.permissions = [x.admin, x.mod];

let cmdRando = new command("tourney", "!randomize", "Forcefully randomizes player seeds", async (data) => {
	dio.del(data.messageID, data);

	// Check for an actual tournament
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	await client.participants.randomize(currentTourney);

	dio.say("🕑 Randomized Player Seeds!", data);
});

cmdRando.permissions = [x.admin, x.mod];

// Non-admin commands

let cmdAdd = new command("tourney", ["!signup","!signin"], "Adds player to tournament", (data) => {
	dio.del(data.messageID, data);

	const cid = (data.args[1]) ? data.args[1] : false; // Challonge username (optional)

	// Check for an actual tournament
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Check if you already signed up
	if (tPlayers.hasOwnProperty(data.userID) && helpers.isHeroku() ) {
		dio.say("🕑 You've already signed up. :tada:", data, tourneyChan);
		return false;
	}

	// Otherwise, if we're under 10 participants, add to tournament
	if (tCount < 10) {
		addPlayer(data, cid);
	} else {
		dio.say("🕑 The tournament has reached the maximum number of entries. Hope to see you next week!", data, tourneyChan);
	}
});

let cmdDel = new command("tourney", ["!unsignup","!signdown","!signout"], "Removes player to tournament", (data) => {
	dio.del(data.messageID, data);

	// Check for an actual tournament
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Check if you already signed up
	if (!tPlayers.hasOwnProperty(data.userID)) {
		dio.say("🕑 You're not even part of the tournament. :expressionless:", data, tourneyChan);
		return false;
	}

	deletePlayer(data, tPlayers[data.userID], data.userID);
});

let cmdCheckIn = new command("tourney", "!checkin", "Checks In to tournament", (data) => {
	dio.del(data.messageID, data);
	tourneyCheckIn(data);
});

let cmdCheckOut = new command("tourney", "!checkout", "Checks Out of tournament", (data) => {
	dio.del(data.messageID, data);
	tourneyCheckIn(data, false);
});

let cmdUpdateScore = new command("tourney", "!score", "Updates score in tournament", (data) => {
	if (!currentTourney) {
		dio.say("🕑 There are no Pocketbot Cups currently running. :thinking: Try again some other time!", data);
		return false;
	}

	// Check if this was done via DM or not
	if ( helpers.getDMChannel(data.bot, data.userID) ) {
		let dm = helpers.getDMChannel(data.bot, data.userID);

		if (data.channelID !== dm) {
			dio.say("🕑 Sorry, but could you DM me the score? That way we can avoid cluttering the channel. :blush:", data);
			return false;
		}

		// Check if we're looking for files from this person anyway
		if (helpers.isHeroku() && !tPlayers.hasOwnProperty(data.userID) ) {
			dio.say("🕑 Uhm...you don't seem to be in the tournament, so I'm going to go ahead and ignore your file. :sweat_smile:", data, data.userID);
			return false;
		}

		// Check if message is missing attachment
		if (data.attachments.length === 0) {
			dio.say("There was no file attached. Make sure to use this command in the comment portion of the **file upload dialog** when attaching the replay.", data, data.userID);
			return false;
		}

		// If someone in the tournament DOES upload a file
		let file = data.attachments[0];
		//console.log(file);

		// If file is not a zip (or rar)
		if (!file.filename.endsWith(".zip") && !file.filename.endsWith(".rar")) {
			dio.say("Please attach a zip with ALL of your replays for your current set.", data, data.userID);
			logger.log(`${file.filename} is not a zip or rar.`,"Error");
			return false;
		}

		// Check size to have a minimum, since it should be 3 replay files, but ignore in debug mode
		if (helpers.isHeroku() && file.size <= 70000) {
			dio.say("This is a suspiciously small zip for 3 replays. Are you sure you included all the files? If so, ping Masta or a mod for help.", data, data.userID);
			return false;
		}

		updateScore(data, file);
	} else {
		dio.del(data.messageID, data);
		dio.say("🕑 I have DM'd you the information you need to update your score. :thumbsup:", data, tourneyChan);
		dio.say(stripIndents`Hey! To update your score, drag and drop a zip file containing the replays for your current set.

			When the file upload dialog pops up, enter in the command syntax: \`!score x-y \` where:

			\`x-y\` is the score, \`x\` is **your** score, \`y\` is your opponent's, e.g. 2-1 or 0-3.

			**NOTE:** If you make a mistake, contact <@${x.stealth}> or a mod.`, data, data.userID);
	}
});

let cmdBracket = new command("tourney", "!bracket", "Link to the current brackets", (data) => {
	dio.del(data.messageID, data);
	dio.say(`:trophy: The current tournament bracket can be found at: http://pocketbotcup.challonge.com/pocketbotcup_${tNum}`, data);
});

let cmdUp = new command("tourney", ["!addreplays","!replay"], "Manual replay upload processing", (data) => {
	checkZip(data, true, true);
});

module.exports.commands = [cmdCreate, cmdAdd, cmdDel, cmdStart, cmdEnd, cmdCheckIn, cmdCheckOut, cmdList, cmdAddMatch, cmdUpdateScore, cmdCheckRound, cmdCheckInRemind, cmdBracket, cmdListReplays, cmdDQ, cmdRando, cmdUp];
