/* ----------------------------------------
	This file controls all the stuff related
	to the Worthless Internet Point currency.
 ---------------------------------------- */

let command = require("../core/command").Command,
	helpers = require("../core/helpers"),
	dio     = require("../core/dio"),
	vars 	= require("../core/vars");

let cmdWip = new command("economy", "!wip", "Check or create your WIP account with the default WIP amount", function(data){
	let udata = data.userdata;

	// Oh snap, PROMISES.
	udata.getProp(data.userID, "currency").then( (res) => {
		if( res || res === 0) {
			dio.say(`:bank: My records say you have **${res}** ${vars.emojis.wip} coins`, data);
		} else {
			let bank = udata.setProp({
				user: data.userID,
				prop: {
					name: "currency",
					data: udata.DEFAULT_CURRENCY_AMOUNT
				}
			});

			// Set default emotes for veteran people
			if ( data.bot.servers[vars.chan].members[data.userID].roles.includes(vars.member) ) {
				udata.setProp({
					user: data.userID,
					prop: {
						name: "emotes",
						data: ["facefeel","fixit","lol","nerfit","rekt", "suchcode", "theworst","trust","yomama","dbqm","durghammer"]
					}
				});
			}

			dio.say(`🕑 <@${data.userID}>, your account has been added to my records. You now have ${bank} Worthless Internet Points™.`, data);
		}
	});
});

let cmdTransfer = new command("economy", "!transfer", "Sends a user a certain amount of currency: `!transfer @recipient amount`, where amount > 0", function(data){
	if(!data.args[1] || !data.args[2]){
		dio.say("The command syntax is `!transfer @recipient amount`", data);
		return;
	}

	if(parseInt(data.args[2]) <= 0){
		dio.say("Amount cannot be negative or zero", data);
		return;
	}

	let recipient = helpers.getUser(data.args[1]),
		amount = parseInt(data.args[2]);

	if(recipient == data.userID){
		dio.say(`Cannot transfer ${vars.emojis.wip} to yourself`, data);
		return;
	}

	data.userdata.transferCurrency(data.userID, recipient, amount, false).then( () => {
		dio.say(`${amount} ${vars.emojis.wip} sent successfully`, data);
	}).catch( (err) => {
		dio.say(err, data);
	});
});

let cmdGive = new command("economy", "!give", "Give a user a certain amount of WIP. `!give @recipient amount`", function(data){
	if(!data.args[1] || !data.args[2]){
		dio.say("The command syntax is `!give @recipient amount`", data);
		return;
	}

	let recipient = helpers.getUser(data.args[1]),
		amount = parseInt(data.args[2]);

	data.userdata.transferCurrency(null, recipient, amount, true).then( (res) => {
		if( res.hasOwnProperty("err") ){
			dio.say(res.err, data);
		}else{
			if(amount >= 0) dio.say(`${amount} ${vars.emojis.wip} ${(amount==1?"was":"were")} generously given to <@${recipient}> by The Great Bank of Narkamertu`, data);
			else dio.say(`${-amount} ${vars.emojis.wip} ${(-amount==1?"was":"were")} taken back from <@${recipient}> by The Great Bank of Narkamertu`, data);
		}
	});
});

function getBuyList() {
	let buylist = ["t1blobs","t2blobs"].concat(vars.gifemotes);
	vars.emotes.forEach( (emote)=> {
		if (!emote.includes("blob")) buylist.push(emote);
	});

	let t1blobs = ["blobsquirrel","bloblizard","blobtoad","blobpigeon","blobmole"],
		t2blobs = ["blobcham","blobferret","blobfalcon","blobskunk","blobsnake"],
		t3blobs = ["blobwolf","blobbadger","blobowl","blobfox","blobboar"];

	return {
		list: buylist,
		t1: t1blobs,
		t2: t2blobs,
		t3: t3blobs
	};
}

let cmdShop = new command("economy", "!shop", "Buy stuff from the shop `!shop buy <item>`", function(data) {
	dio.del( data.messageID, data);

	// Make the purchase list
	let buy = getBuyList();

	// Prep user
	let mem = data.bot.servers[vars.chan].members[data.userID],
		fromRoles = [];

	if (mem) fromRoles = (data.bot.servers[vars.chan].members[data.userID].roles) || null;

	if (!data.args[1]) {
		dio.say("🕑 Use one of three commands to work the shop: `!shop examples`, `!shop buy <item>`, or `!shop list`" ,data);
		return false;
	}

	// Sub commands
	switch(data.args[1]) {
	case "examples":
		dio.sendImage("assets/shopExamples.png", data, "Here are examples of our current items!\nTo see the item IDs, use the syntax `!shop list`", false, data.userID);
		break;
	case "buy":
		// If you forgot the item name
		if (!data.args[2]) {
			dio.say("🕑 Did you forget to choose an item? Syntax is `!shop buy <item>`" ,data);
			return false;
		}
		// If the item is there, check if it exists in the list
		if ( !buy.list.includes(data.args[2]) ) {
			dio.say("🕑 I don't recognize this item. Double check the list with `!shop list`" ,data);
			return false;
		}
		// If the user is a Mod/Dev
		if ( fromRoles.includes(vars.mod) || fromRoles.includes(vars.admin) ) {
			dio.say("🕑 You already have access to all the stuff!" ,data);
			return false;
		}

		// At this point it should be a legit purchase, time to access Firebase data
		// =========================================================================
		data.db.soldiers.child(data.userID).once("value", function(snap) {
			let user = snap.val(),
				cost;

			// Set the cost of the desired item
			if ( data.args[2].includes("blobs") ) {
				cost = 15;
			} else if ( vars.gifemotes.includes(data.args[2]) ) {
				cost = 20;
			} else { cost = 5; }

			// Check if user has the cash
			if (user.hasOwnProperty("currency")) {
				if (user.currency < cost) {
					dio.say("🕑 You don't have enough WIP to purchase this item.", data);
					return false;
				}
			} else {
				dio.say(`🕑 I don't think you've activated your WIP account yet! Go to <#${vars.playground}> and type in \`!wip\` to get started.`, data);
				return false;
			}

			// Check emote aliases
			let emoteAlias = data.args[2];

			// Blob Check
			if (emoteAlias.includes("blobs")) {
				switch(emoteAlias) {
				case "t1blobs":
					emoteAlias = "blobtoad";
					break;
				case "t2blobs":
					emoteAlias = "blobcham";
					break;
				case "t3blobs":
					emoteAlias = "blobwolf";
					break;
				}
			}

			// Check if user has ANYTHING unlocked
			if ( user.hasOwnProperty("emotes") ) {
				let myEmotes = Object.values(user.emotes);

				// If he already has this specific one
				if (myEmotes.includes(emoteAlias) ) {
					dio.say("🕑 You already have access to this item!", data);
					return false;
				} else { // If he has emotes, but not this one...
					dio.say(`🕑 You have now bought this item and have ${user.currency - cost} WIP left.`, data);

					if (data.args[2] === emoteAlias) {
						myEmotes.push(data.args[2]);
					} else {
						if (emoteAlias === "blobtoad") myEmotes = myEmotes.concat(buy.t1);
						if (emoteAlias === "blobcham") myEmotes = myEmotes.concat(buy.t2);
						if (emoteAlias === "blobwolf") myEmotes = myEmotes.concat(buy.t3);
					}

					data.db.soldiers.child(data.userID).update({
						emotes: myEmotes,
						currency: user.currency - cost
					});
					return false;
				}
			} else {
				// If he has NO emotes...
				dio.say(`🕑 You have now bought this item and have ${user.currency - cost} WIP left.`, data);
				let myEmotes = [];

				if (data.args[2] === emoteAlias) {
					myEmotes.push(data.args[2]);
				} else {
					if (emoteAlias === "blobtoad") myEmotes = myEmotes.concat(buy.t1);
					if (emoteAlias === "blobcham") myEmotes = myEmotes.concat(buy.t2);
					if (emoteAlias === "blobwolf") myEmotes = myEmotes.concat(buy.t3);
				}

				data.db.soldiers.child(data.userID).update({
					emotes: myEmotes,
					currency: user.currency - cost
				});
				return false;
			}
		});

		break;
	case "list":
		dio.say(`Here's a list of all the crap you can buy: \`\`\`${buy.list} \`\`\` \nTo buy one of the items, use the syntax \`!shop buy <item>\``, data, data.userID);
		break;
	}
});

cmdGive.permissions = [vars.mod, vars.admin];

module.exports.commands = [cmdWip, cmdTransfer, cmdGive, cmdShop];
